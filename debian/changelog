nexus (4.4.3-1) UNRELEASED; urgency=medium

  * New upstream release.
    - drop patches incorporated upstream.
    - drop patches for old build system.
    - change to cmake build system.
    - drop python-nxs package that is now a separate source (Closes: #896357,
      #937134).
    - stop building against MXML 3.0 until compatibility can be restored
      (Closes: #940108).
    - patch build system to handle newer Java (Closes: #893383).
  * Use debhelper-compat 12.
  * Update Standards-Version to 4.4.0 (no changes required).
  * Add myself to Uploaders.

 -- Stuart Prescott <stuart@debian.org>  Sun, 15 Sep 2019 11:32:12 +1000

nexus (4.3.2-svn1921-6) unstable; urgency=medium

  * Team upload

  [ Andreas Tille ]
  * Switch Build-Depends from libhdf4g-dev to libhdf4-dev
    Closes: #866925

  [ Gilles Filippini ]
  * Fix patch hdf5-1.10-support.patch (closes: #861736)

 -- Gilles Filippini <pini@debian.org>  Wed, 01 Nov 2017 19:30:46 +0100

nexus (4.3.2-svn1921-5) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Take over package into Debian Science team
  * Fix wrong Priority: extra -> optional
  * Add watch file
  * Add Homepage field

  [ Gilles Filippini ]
  * New patch hdf5-1.10-support.patch to fix incorrect type for HDF5
    handles: should be hid_t instead of int (closes: #861736)

 -- Andreas Tille <tille@debian.org>  Thu, 11 May 2017 09:23:17 +0200

nexus (4.3.2-svn1921-4) unstable; urgency=medium

  * QA upload.
  * New patch fix-hdf5-1-10-detection to support DF5 1.10 (closes: #841965)

 -- Gilles Filippini <pini@debian.org>  Fri, 22 Jul 2016 23:05:27 +0200

nexus (4.3.2-svn1921-3) unstable; urgency=medium

  * QA upload.
  * Orphan the package, see #805446.
  * debian/control: bump Standards-Version to 3.9.6, no changes needed
  * run wrap-and-sort
  * Build-Depend on dh-python instead of python-support.  Looks like
    dh_python2 was already used in d/rules anyway.  Closes: #786316
  * Remove debian/gbp.conf, since this package is not maintained in git.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 13 Dec 2015 23:08:45 +0000

nexus (4.3.2-svn1921-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename library packages for g++5 ABI transition (closes: 791217).

 -- Julien Cristau <jcristau@debian.org>  Sun, 16 Aug 2015 17:47:53 +0200

nexus (4.3.2-svn1921-2) unstable; urgency=low

  * Support hdf5 1.8.13 new packaging layout.
    Patch by Gilles Filippini <pini@debian.org>
    (Closes: #756688)

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Fri, 1 Aug 2014 00:21:53 +0100

nexus (4.3.2-svn1921-1) unstable; urgency=low

  * disginguish between static and dynamic linking, fix
    from upstream (Closes: #743480)
  * provide correct pkg-config version number

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Wed, 16 Apr 2014 19:21:53 +0100

nexus (4.3.2-svn1919-2) unstable; urgency=medium

  * add missing dependencies for -dev package
  * bump standards version (no modification)

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Thu, 03 Apr 2014 17:00:12 +0100

nexus (4.3.2-svn1919-1) unstable; urgency=low

  * upstream: fix problem with null terminated fixed size strings in nxbrowse
  * force building with hdf5 (ubuntu managed to build without)

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Wed, 13 Nov 2013 14:36:50 +0000

nexus (4.3.2-svn1915-1) unstable; urgency=low

  * new upstream snapshot
  * better support from compression from Java
  * bug fixes

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Sat, 14 Sep 2013 18:19:10 +0100

nexus (4.3-svn1898-1) unstable; urgency=low

  * updated upstream version fixing NX5getinfo64

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Sat, 22 Jun 2013 16:20:31 +0100

nexus (4.3-svn1896-1) unstable; urgency=low

  * new upstream version to resolve issues around napiconfig.h
    and add various small improvements

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Sun, 19 May 2013 18:15:22 +0100

nexus (4.3-svn1889-1) unstable; urgency=low

  * new upstream release with bug fixes
  * ensure backward compatibility when compiling with Java 7

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Fri, 08 Mar 2013 00:30:22 +0000

nexus (4.3-svn1863-2) unstable; urgency=low

  * conflict with old python package to allow upgrade
  * populate binary-indep target for python
  * bump standards version (potential symbols work outstanding)

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Sun, 17 Feb 2013 19:45:47 +0000

nexus (4.3-svn1863-1) unstable; urgency=low

  * new upstream version (Closes: #692425)
  * use dpkg-buildflags

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Thu, 13 Dec 2012 15:29:09 +0000

nexus (4.3-svn1815-2) unstable; urgency=low

  * remove transitional libhdf5-serial-dev dependency

 -- Tobias Richter <tsr-debian@achos.com>  Wed, 23 May 2012 10:05:18 +0100

nexus (4.3-svn1815-1) unstable; urgency=low

  * new upstream version
  * update standards version from 3.9.2 to 3.9.3 (no change)
  * make python arch all (Closes: #662215)
  * better python dependency (Closes: #662214)
  * correct python package name (Closes: #662158)
  * Thanks to Jakub for spotting all my python packaging mistakes!

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Mon, 14 May 2012 18:22:36 +0100

nexus (4.2.1-svn1614-2) unstable; urgency=low

  * better solution for finding JAVA_HOME (Closes: #636203)
  * provide build-arch and build build-indep targets

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Tue, 02 Aug 2011 11:37:59 +0100

nexus (4.2.1-svn1614-1) unstable; urgency=low

  * Initial release (Closes: #411053)

 -- Tobias Stefan Richter <tsr-debian@achos.com>  Thu, 21 Jul 2011 11:52:11 +0100
